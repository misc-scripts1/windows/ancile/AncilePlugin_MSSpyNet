# Ancile Microsoft SpyNet
### About
https://gitlab.com/misc-scripts1/windows/ancile/AncilePlugin_MSSpyNet

This plugin disables Microsoft's SpyNet telemetry reporting. 

This is a plugin that requires Ancile_Core (https://gitlab.com/misc-scripts1/windows/ancile/Ancile_Core) 

For more information go to https://voat.co/v/ancile